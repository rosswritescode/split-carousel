/**
 * Site split class
 */
export function Split({
	holder = null,
	navLinksSelector = null,
	panelClass = null,
	transitionClass = 'split-panel__split--left',
	shownClass = 'split-panel--show',
	hidingClass = 'split-panel--hiding'
} = {}) {
	let split = this;

	// Test for required elements.
	if(!holder || !navLinksSelector || !panelClass) { 
		return false;
	}

	// Set elems
	split.holder = holder;
	split.navLinks = split.holder.querySelectorAll(navLinksSelector);
	split.shownPanel = null;
	split.previousPanel = null;

	// Set attributes
	split.panelClass = panelClass;
	split.shownClass = shownClass;
	split.hidingClass = hidingClass;
	split.transitionClass = transitionClass;

	// Init map
	split.linkPanelsMap = new Map();

	// Init this class
	split.init();
}

/**
 * Public functions.
 */

// Init the elements, parse links, set events
Split.prototype.init = function () {
	let split = this;

	// Loop and parse links
	let navLinksArray = [...split.navLinks];
	navLinksArray.forEach(initNavLink.call(split));

	var event = new Event('click');
	navLinksArray[0].dispatchEvent(event);
}

Split.prototype.goToPanel = function(showPanelObject) {
	let split = this;
	let showPanel = showPanelObject.linkPanel;
	let shownClass = showPanelObject.selectedClass;
	
	// Add hiding class to currently shown panel
	if(split.shownPanel !== null) {
		split.shownPanel.classList.add(split.hidingClass);
		split.shownPanel.classList.remove(split.shownClass);

		// Make this our previous panel
		split.previousPanel = split.shownPanel;
	}
	
	// Make sure our new panel isnt hidden, and add shown class.
	showPanel.classList.remove(split.hidingClass);
	showPanel.classList.add(split.shownClass);

	// Set our newly shown panel
	split.shownPanel = showPanel;
	split.clearSelectedClass();
	split.holder.classList.add(shownClass);

	// Activate any links
	split.activateLinks(showPanel);
}

Split.prototype.activateLinks = function (panel) {
	let split = this;

	// Deactivate any links in panels
	[...split.holder.querySelectorAll(`.${split.panelClass} a`)].forEach(link => link.setAttribute('tabIndex', -1));

	// Only activate links in this panel.
	[...panel.querySelectorAll(`a`)].forEach(link => link.removeAttribute('tabIndex'));	
}

Split.prototype.clearSelectedClass = function (panel) {
	let split = this;

	for (var value of split.linkPanelsMap.values()) {
		split.holder.classList.remove(value.selectedClass);
	}
}

/**
 * Private functions.
 */

// Get the node linked to by the link provided.
function parsePanelNode(link) {
	let split = this;

	// Get data required
	let linkHref = link.getAttribute('href');
	let linkTarget = split.holder.querySelector(linkHref);
	let linkPanel = linkTarget.parentNode;
	
	// Add to map and create dupe content
	createDuplicateContent.call(split, linkPanel);
	setPanelMap.call(split, link, linkPanel, linkTarget);

	// Add listener
	linkPanel.querySelector(`.${split.transitionClass}`).addEventListener('transitionend', onTransitionend.call(split));
}

function setPanelMap(link, linkPanel, linkTarget) {
	let split = this;

	let linkObject = {
		selectedClass: linkTarget.getAttribute('data-split-selected-class'),  // TODO variable data selector
		linkPanel: linkPanel
	};

	split.linkPanelsMap.set(link, linkObject);
}

// Duplicate content with JS so you just need to write one copy of the content to HTML
function createDuplicateContent(linkPanel) {
	let split = this;

	// The panels content
	let panelContent = linkPanel.querySelector('.split-panel__content'); // TODO variable class
	panelContent.parentNode.removeChild(panelContent);

	// Create a split holder
	let splitHolder = document.createElement('div');
	let splitContent = document.createElement('div');
	splitHolder.classList.add('split-panel__split');  // TODO variable class
	splitContent.classList.add('split-panel__content');  // TODO variable class

	// Combine elements for one split holder.
	splitContent.appendChild(panelContent);
	splitHolder.appendChild(splitContent);

	// CLone split holder, one for the left, one for the right.
	let leftSplitHolder = splitHolder.cloneNode(true);
	let rightSplitHolder = splitHolder.cloneNode(true);
	leftSplitHolder.classList.add('split-panel__split--left');  // TODO variable class
	rightSplitHolder.classList.add('split-panel__split--right');  // TODO variable class
	
	// Add both to parent.
	linkPanel.appendChild(leftSplitHolder);
	linkPanel.appendChild(rightSplitHolder);
}

// Add click event to the link
function addClickEvent(link) {
	let split = this;

	link.addEventListener('click', linkClick.call(split));
}

// Events functions.
function linkClick() {
	let split = this;

	return function doLinkClick(e) {
		let linkObject = split.linkPanelsMap.get(e.target);
	
		split.goToPanel(linkObject);
	
		e.preventDefault();
	}
}

function initNavLink () {
	let split = this;

	return function doInitNavLink(link) {
		parsePanelNode.call(split, link);
		addClickEvent.call(split, link);
	}
}

function onTransitionend () {
	let split = this;

	return function doOnTransitionend(e) {
		e.target.parentNode.classList.remove(split.hidingClass);
	}
}
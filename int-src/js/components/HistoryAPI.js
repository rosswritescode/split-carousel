
// Private variables shared between all instances of History API
let IS_HISTORY_INIT = false;
let CALLBACKS = [];

/**
 * History API class
 */
export function HistoryAPI() {
	let historyApi = this;

	// We only do this once.
	if(!IS_HISTORY_INIT) {
		initPopState();

		IS_HISTORY_INIT = true;
	}
}

/**
 * Public functions.
 */
HistoryAPI.prototype.add = function (linkTarget) {
	history.pushState({
		page: linkTarget
	}, `${linkTarget}`, `#/${linkTarget}`);
}

HistoryAPI.prototype.addPopStateCallback = function (callback) {
	// Add to global callbacks
	CALLBACKS.push(callback);

	// See if we need to do anything initially.
	checkURL();
}


/**
 * Private functions.
 */

function checkURL() {
	let hashFragment = window.location.hash;
	if(hashFragment) {
		let page = hashFragment.split('#/')[1];
		if(page) {
			// Timeout for Edge/IE - didnt fire transitionend on init load.
			setTimeout(() => {
				fireCallbacks(page);
			}, 10);
		}
	}
}

function initPopState () {
	// Change to listener?
	window.onpopstate = function(e) {
		if(e.state) {
			fireCallbacks(e.state.page);
		} else {
			// check URL for #/ identifier
			checkURL();
		}
	}
}

function fireCallbacks(hashValue) {
	CALLBACKS.forEach(callback => callback(hashValue));
}

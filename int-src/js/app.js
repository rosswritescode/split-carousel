import { addJsClass } from 'addJsClass';
import { Split } from 'Split';

document.addEventListener("DOMContentLoaded", function initPage(event) {
	// Add js class to body
	addJsClass(); 

	[...document.querySelectorAll('.split')].map(function(splitHolder) {

		// Init Split
		let splitObject = new Split({
			holder: splitHolder,
			navLinksSelector: 'nav > a[href^="#"]',
			panelClass: 'split-panel'
		});

	});
	
});
const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const server = require('gulp-server-livereload');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');

const assemble = require("assemble");
const rename = require("gulp-rename");
const cached = require("gulp-cached");
const remember = require("gulp-remember");
const util = require('gulp-util');

const isProduction = util.env.production;

gulp.task('js', function () {
		return browserify({
			paths: [
				"int-src/js/objects",
				"int-src/js/helpers",
				"int-src/js/components"
			],
			entries: './int-src/js/app.js',
			extensions: ['.jsx'],
			debug: !isProduction
		})
		.transform(babelify.configure({
			presets: [
				"es2015"
			]
		}))
		.bundle()
		.pipe(source('all.min.js'))
		.pipe(gulp.dest('int-build/js'));
});


gulp.task('js-polyfills', function () {
	return gulp.src('./int-src/js/polyfills/*.js')
	.pipe(concat('polyfills.js'))
	.pipe(gulp.dest('int-build/js'));
});

gulp.task('images', function () {
	return gulp.src([
		'./int-src/images/**/*.png',
		'./int-src/images/**/*.jpg'
	])
	.pipe(gulp.dest('int-build/images'));
});

gulp.task('sass', function () {
	return gulp.src('./int-src/css/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('main.css'))
		.pipe(gulp.dest('./int-build/css'));
});

//  ============================================================================================================
//  Tasks
//  ============================================================================================================
var app = assemble();

// Prepare HBS Files
gulp.task("prepare-assemble", function prepareAssemble(complete) {
	app.layouts("int-src/html/layouts/**/*.hbs");
	app.partials("int-src/html/partials/**/*.hbs");
	app.pages("int-src/html/pages/**/*.hbs");
	complete();
});

// Render Pages
gulp.task("html", ["prepare-assemble"], function assemble() {
	return app.toStream("pages")
		.pipe(app.renderFile({ "flatten": false }))
		.pipe(cached("assemble"))
		.on("error", console.log)
		.pipe(rename({ extname: ".html" }))
		.pipe(remember("assemble"))
		.pipe(app.dest("int-build"));
});


gulp.task('webserver', function() {
	gulp.src('./int-build')
	  .pipe(server({
		livereload: true,
		open: true
	  }));
  });

gulp.task('watch:dev', function () {
	gulp.watch('int-src/css/**/*.scss', ['sass']);
	gulp.watch('./int-src/js/**/*.js', ['js']);
	gulp.watch('./int-src/html/**/*.hbs', ['html']);
});

